$(document).ready(function () {
	/*------ Hero slider active 1 ----*/
	$('.hero-slider-active-1').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		loop: true,
		dots: true,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
	});

	/*------ Hero slider active 2 ----*/
	$('.hero-slider-active-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		loop: true,
		dots: false,
		arrows: true,
		adaptiveHeight: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
	});

	/*------ Hero slider active 3 ----*/
	$('.hero-slider-active-3').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		loop: true,
		dots: true,
		arrows: false,
	});

	/*------ Product slider active ----*/
	$('.product-slider-active').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		arrows: false,
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ Product slider active 2 ----*/
	$('.product-slider-active-2').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		rows: 2,
		arrows: false,
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ Product slider active 3 ----*/
	$('.product-slider-active-3').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ Product slider active 4 ----*/
	$('.product-slider-active-4').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ Product slider active 5 ----*/
	$('.product-slider-active-5').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: false,
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ product categories slider 1 ----*/
	$('.product-categories-slider-1').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});

	/*------ Product categories slider 3 ----*/
	$('.product-categories-slider-3').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		rows: 2,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});

	/*--------------------------------
	Deal slider active
	-----------------------------------*/
	$('.deal-slider-active').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
	});


	/*--------------------------------
	Sidebar product active
	-----------------------------------*/
	$('.sidebar-product-active').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		rows: 3,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
	});

	/*--------------------------------
	Sidebar blog active
	-----------------------------------*/
	$('.sidebar-blog-active').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		rows: 2,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
	});

	/*--------------------------------
	Product categories slider
	-----------------------------------*/
	$('.product-categories-slider-2').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
			breakpoint: 1199,
			settings: {
				slidesToShow: 4,
				}
			},
			{
			breakpoint: 991,
			settings: {
				slidesToShow: 3,
				}
			},
			{
			breakpoint: 767,
			settings: {
				slidesToShow: 3,
				}
			},
			{
			breakpoint: 575,
			settings: {
				slidesToShow: 2,
				}
			}
		]
	});

	/*--------------------------------
	Testimonial active
	-----------------------------------*/
	$('.testimonial-active-1').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		arrows: false,
	});
	/*--------------------------------
	Testimonial active 2
	-----------------------------------*/
	$('.testimonial-active-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: false,
	});

	/*--------------------------------
	Product slider active 6
	-----------------------------------*/
	$('.product-slider-active-6').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		rows: 2,
		arrows: false,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 1,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*--------------------------------
	Product slider active 7
	-----------------------------------*/
	$('.product-slider-active-7').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		rows: 2,
		arrows: false,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*--------------------------------
	Product slider active 8
	-----------------------------------*/
	$('.product-slider-active-8').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 4,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*--------------------------------
	Product slider active 9
	-----------------------------------*/
	$('.product-slider-active-9').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: true,
		arrows: false,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	$('.pro-dec-big-img-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		draggable: false,
		fade: false,
		asNavFor: '.product-dec-slider-small , .product-dec-slider-small-2',
	});

	/*---------------------------------------
	Product details small image slider
	-----------------------------------------*/
	$('.product-dec-slider-small').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.pro-dec-big-img-slider',
		dots: false,
		focusOnSelect: true,
		fade: false,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [{
			breakpoint: 991,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 3,
				}
			}
		]
	});

	/*----------------------------------------
	Product details small image slider 2
	------------------------------------------*/
	$('.product-dec-slider-small-2').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		vertical: true,
		asNavFor: '.pro-dec-big-img-slider',
		dots: false,
		focusOnSelect: true,
		fade: false,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [{
			breakpoint: 1365,
			settings: {
					slidesToShow: 4,
				}
			},
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 4,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 4,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 2,
				}
			}
		]
	});
	$('.related-product-active').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		fade: false,
		loop: true,
		dots: false,
		arrows: false,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	/*------ Hero slider active 2 ----*/
	$('.category-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		loop: true,
		lazyLoad: 'ondemand',
		dots: true,
		arrows: true,
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});
	$('.partners-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		loop: true,
		dots: true,
		arrows: true,
		lazyLoad: 'ondemand',
		prevArrow: '<span class="slider-icon-1-prev"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 1.5L2 9.5L10 17.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		nextArrow: '<span class="slider-icon-1-next"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L9 9.5L1 1.5" stroke="#454545" stroke-width="2" stroke-linecap="round"/></svg></span>',
		responsive: [
			{
			breakpoint: 1199,
			settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 991,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 767,
			settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 575,
			settings: {
					slidesToShow: 1,
				}
			}
		]
	});
});
